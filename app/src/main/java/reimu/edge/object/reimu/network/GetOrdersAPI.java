package reimu.edge.object.reimu.network;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

import reimu.edge.object.reimu.utils.HttpUtils;
import reimu.edge.object.reimu.utils.OrderHttpUtils;

/**
 * Created by owner on 3/13/18.
 */

public class GetOrdersAPI {



    private static final String TAG = GetOrdersAPI.class.getSimpleName();
    private String orderDetails;

    public JSONArray getOrdersData() {
        JSONArray jsonArray = null;
        try {
            orderDetails = OrderHttpUtils.processOrderRequestandGetResponse(new URL("http://192.168.178.238:7003/pricing/objectedge9/order/getorders/?login=johndoe@yopmail.com&userPass=password&userID=400000"));
            jsonArray = new JSONArray(orderDetails);

        } catch (Exception exception) {
            Log.e(TAG, exception.getMessage());
        }

        return jsonArray;
    }
}
