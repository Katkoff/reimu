package reimu.edge.object.reimu.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import reimu.edge.object.reimu.AccountActivity;
import reimu.edge.object.reimu.LandingActivity;
import reimu.edge.object.reimu.R;
import reimu.edge.object.reimu.adapters.MyAccountAdapter;
import reimu.edge.object.reimu.asynctasks.LoginAsyncTask;
import reimu.edge.object.reimu.asynctasks.OrderAsyncTask;
import reimu.edge.object.reimu.asynctasks.OrderDetailsAsyncTask;
import reimu.edge.object.reimu.model.CommerceItems;
import reimu.edge.object.reimu.model.OrderDetailsModel;

public class MyAccountFragment extends Fragment implements OrderAsyncTask.IOrdersCallBack {

    private ArrayList<CommerceItems> mOrderDetailsModelList;
    private ListView mOrdersListView;
    private JSONArray mOrders;
    private TextView mSubscriptionAmount;
    private ProgressDialog mProgressDialog;
    private View mRootView;

    public static MyAccountFragment newInstance() {
        MyAccountFragment fragment = new MyAccountFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.fragment_my_account, container, false);
        mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", true);
        mOrdersListView = (ListView) mRootView.findViewById(R.id.my_account_subscriptions_list);
        mSubscriptionAmount = (TextView) mRootView.findViewById(R.id.subscriptionTotalAmount);
        mRootView.setVisibility(View.INVISIBLE);
        mOrderDetailsModelList = new ArrayList<>();

        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        OrderAsyncTask orderAsyncTask = new OrderAsyncTask(this);
        orderAsyncTask.execute();

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onOrdersDataCallBack(JSONArray orders) {

        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        mRootView.setVisibility(View.VISIBLE);
        calculateSubsciptionAmount(retrieveSubscriptionAmount(orders));

        if ((mOrderDetailsModelList != null)) {
            MyAccountAdapter myAccountAdapter = new MyAccountAdapter(getLayoutInflater(), mOrderDetailsModelList);
            mOrdersListView.setAdapter(myAccountAdapter);
        }


    }

    private float retrieveSubscriptionAmount(JSONArray orders) {
        Gson gson = new Gson();
        float subscriptionTotal = 0.0f;

        OrderDetailsModel[] orderDetailsModel = gson.fromJson(orders.toString(), OrderDetailsModel[].class);
        for (OrderDetailsModel orderDetailsModel1 : orderDetailsModel) {
            for (CommerceItems commerceItems : orderDetailsModel1.getCommerceItems()) {
                mOrderDetailsModelList.add(commerceItems);
                subscriptionTotal += Float.valueOf(commerceItems.getPriceInfo().getAmount());

            }
        }
        return subscriptionTotal;
    }


    private void calculateSubsciptionAmount(float amount) {
        String subAmount = (String) mSubscriptionAmount.getText();
        subAmount = subAmount.replace("34.98", String.valueOf(amount));
        mSubscriptionAmount.setText(subAmount);
    }


}
