package reimu.edge.object.reimu.seekbar;


/**
 * Created by michaelkatkov on 4/13/18.
 */

public interface SeekBarValueChangedListener
{
    void onValueChanged(final int position, final boolean thumbPressed);
}
