package reimu.edge.object.reimu.fragments;


import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Gravity;
import android.webkit.*;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import reimu.edge.object.reimu.R;


/**
 * Created by michaelkatkov on 4/13/18.
 */

public class PopupView extends RelativeLayout {

    private WebView mWebView;
    private ImageView mCloseBtn;

    public PopupView(Context context) {
        super(context);
        initVieW();
    }


    public PopupView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initVieW();
    }


    public PopupView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initVieW();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PopupView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initVieW();
    }

    private void initVieW() {
        inflate(getContext(), R.layout.add_popup, this);
        mWebView = findViewById(R.id.web_view);
        mCloseBtn = findViewById(R.id.close_button);
    }

    public void showAdd(String url, OnClickListener listener) {
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.loadUrl(url);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                //  wv.loadUrl("javascript:(function() { document.getElementsByClassName('ytp-large-play-button ytp-button')[0].click(); })()");

            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed(); // Ignore SSL certificate errors
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
            }
        });
        mCloseBtn.setOnClickListener(listener);
    }
}
