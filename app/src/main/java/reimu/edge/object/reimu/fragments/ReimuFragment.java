package reimu.edge.object.reimu.fragments;


import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import reimu.edge.object.reimu.R;
import reimu.edge.object.reimu.seekbar.*;

import java.util.ArrayList;
import java.util.HashSet;


public class ReimuFragment extends Fragment {


    public static final String YOUTUBE_API_KEY = "AIzaSyBb7okZ2kpGvCqCbHCOz0Zwy-RasHhipbA";
    private MyPlaybackEventListener playbackEventListener;
    private MyPlayerStateChangeListener playerStateChangeListener;


    private boolean isAddDisplayed = false;
    private static final int RECOVERY_REQUEST = 1;
    private YouTubePlayer     mPlayer;
    private Handler           handler;
    private TerminalSeekBar   terminalSeekBar;
    private ImageView         playButton;
    private TextView          timerTv;

    public ReimuFragment() {
        // Required empty public constructor
    }


    public static ReimuFragment newInstance() {
        ReimuFragment fragment = new ReimuFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_reimu, container, false);
        terminalSeekBar = view.findViewById(R.id.terminal_seekbar);
        playButton = view.findViewById(R.id.play_button);
        playButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mPlayer.isPlaying()) {
                    if (null != mPlayer && mPlayer.isPlaying())
                        mPlayer.pause();
                } else {
                    if (null != mPlayer && !mPlayer.isPlaying())
                        mPlayer.play();
                }
            }
        });
        timerTv = view.findViewById(R.id.timer_text);
        playbackEventListener = new MyPlaybackEventListener();
        playerStateChangeListener = new MyPlayerStateChangeListener();
        handler = new Handler();
        YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.youtube_layout, youTubePlayerFragment).commit();
        setRetainInstance(true);
        youTubePlayerFragment.initialize(YOUTUBE_API_KEY, new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult youTubeInitializationResult) {
                if (youTubeInitializationResult.isUserRecoverableError()) {
                    youTubeInitializationResult.getErrorDialog(getActivity(), RECOVERY_REQUEST).show();
                }
            }

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {

                if (!wasRestored) {

                    mPlayer = player;
                    mPlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
                    mPlayer.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
                    mPlayer.setPlayerStateChangeListener(playerStateChangeListener);
                    mPlayer.setPlaybackEventListener(playbackEventListener);

                    mPlayer.cueVideo("dXQiE-f9RuM");
                }
            }

        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
       if( mPlayer!=null && !mPlayer.isPlaying() ){
           mPlayer.play();
       }
    }

    private void showAdd(String url) {
        PopupView popupView = new PopupView(getActivity());
        final PopupWindow pw = new PopupWindow(popupView, getActivity().getWindow().getWindowManager().getDefaultDisplay().getWidth(), getActivity().getWindow().getWindowManager().getDefaultDisplay().getHeight(), true);
        popupView.showAdd(url, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pw.dismiss();
                if( mPlayer!=null && !mPlayer.isPlaying() ){
                    mPlayer.play();
                }
            }
        });
        pw.showAtLocation(popupView, Gravity.CENTER, 0, 0);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            displayCurrentTime();
            handler.postDelayed(this, 100);
        }
    };


    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {

        @Override
        public void onBuffering(boolean b) {
            Log.i("", "");

        }

        @Override
        public void onPaused() {
            playButton.setImageResource(R.drawable.ic_play_arrow);
            handler.removeCallbacks(runnable);
        }

        @Override
        public void onPlaying() {
            playButton.setImageResource(R.drawable.ic_pause);
            handler.postDelayed(runnable, 100);
            displayCurrentTime();
        }

        @Override
        public void onSeekTo(int arg0) {
            handler.postDelayed(runnable, 100);
        }

        @Override
        public void onStopped() {
            playButton.setImageResource(R.drawable.ic_play_arrow);
            handler.removeCallbacks(runnable);
        }

    }

    private final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {

        @Override
        public void onLoading() {
        }

        @Override
        public void onLoaded(String s) {
            if( mPlayer!=null && !mPlayer.isPlaying() ){
                mPlayer.play();
            }
            playButton.setVisibility(View.VISIBLE);
            setupSeekBar(mPlayer.getDurationMillis());
        }

        @Override
        public void onAdStarted() {

        }

        @Override
        public void onVideoStarted() {

        }

        @Override
        public void onVideoEnded() {

        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {

        }
    }

    private void setupSeekBar(int duration) {
        final HashSet<Terminal> reachedTerminals = new HashSet<>();
        final ArrayList<Terminal> terminals = new ArrayList<>();
        terminals.add(new Terminal(20000, Color.BLACK, "Ad1" , Terminal.DEFAULT_PRIORITY_NORMAL));
        terminals.add(new Terminal(60000, Color.BLACK, "Ad2", Terminal.DEFAULT_PRIORITY_HIGH));
        terminalSeekBar.setMaxValue(duration);
        terminalSeekBar.setTerminals(terminals);
        terminalSeekBar.enablePriorityBlinking(TerminalAnimationType.BLINK_STILL);
        terminalSeekBar.setTerminalChangedListener(new TerminalChangedListener()
        {
            @Override
            public void onTerminalChanged(Terminal terminal) {
                if (terminal.getInformation().equals("Ad1") && !reachedTerminals.contains(terminal)) {
                    reachedTerminals.add(terminal);
                    mPlayer.pause();
                    showAdd("file:///android_asset/add_video1.html");
                } else if (terminal.getInformation().equals("Ad2") && !reachedTerminals.contains(terminal)) {
                    reachedTerminals.add(terminal);
                    mPlayer.pause();
                    showAdd("file:///android_asset/add_video2.html");
                }
            }
        });

        terminalSeekBar.setSeekBarValueChangedListener(new SeekBarValueChangedListener() {

            @Override
            public void onValueChanged(int position, boolean thumbPressed) {
                if (thumbPressed) {
                    long lengthPlayed = (mPlayer.getDurationMillis() * position) / 100;
                    mPlayer.seekToMillis((int) lengthPlayed);
                }
            }
        });

    }

    private void displayCurrentTime() {
        terminalSeekBar.setProgress(mPlayer.getCurrentTimeMillis());
        String formattedTime = formatTime(mPlayer.getDurationMillis() - mPlayer.getCurrentTimeMillis());
        timerTv.setText(formattedTime);
    }

    private String formatTime(int millis) {
        int seconds = millis / 1000;
        int minutes = seconds / 60;
        return String.format("%02d:%02d", minutes % 60, seconds % 60);
    }
}
