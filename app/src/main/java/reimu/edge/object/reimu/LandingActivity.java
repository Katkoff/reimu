package reimu.edge.object.reimu;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import reimu.edge.object.reimu.adapters.VideoAdapter;


/**
 * Created by michaelkatkov on 4/4/18.
 */

public class LandingActivity extends AppCompatActivity {

    private RecyclerView recyclerViewRecentlyWatched;
    private RecyclerView recyclerViewWatchList;
    private TextView myAccount;
    private ImageView banner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        banner = findViewById(R.id.banner);
        banner.setOnClickListener(videoClick);
        recyclerViewRecentlyWatched = findViewById(R.id.recently_watched_rv);
        recyclerViewWatchList = findViewById(R.id.recently_watchlist_rv);
        myAccount = findViewById(R.id.my_account);
        myAccount.setOnClickListener(onAccountClick);
        setUpRecentlyWatchedRecyclerView();
        setUpWatchListRecyclerView();
    }

    private void setUpRecentlyWatchedRecyclerView() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewRecentlyWatched.setLayoutManager(layoutManager);
        int[] images = new int[] {R.drawable.images1, R.drawable.images2};
        VideoAdapter adapter = new VideoAdapter(images, videoClick);
        recyclerViewRecentlyWatched.setAdapter(adapter);
    }

    private void setUpWatchListRecyclerView() {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewWatchList.setLayoutManager(layoutManager);
        int[] images = new int[] {R.drawable.images1 };
        VideoAdapter adapter = new VideoAdapter(images, videoClick);
        recyclerViewWatchList.setAdapter(adapter);
    }

    private View.OnClickListener videoClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LandingActivity.this, VideoActivity.class);
            startActivity(intent);
        }
    };


    private View.OnClickListener onAccountClick = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LandingActivity.this, AccountActivity.class);
            startActivity(intent);
        }
    };

}
