package reimu.edge.object.reimu.asynctasks;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import reimu.edge.object.reimu.fragments.MyAccountFragment;
import reimu.edge.object.reimu.network.GetOrdersAPI;

/**
 * Created by owner on 3/13/18.
 */

public class OrderAsyncTask extends AsyncTask<Void,Void,JSONArray>{

    public interface IOrdersCallBack {
        public void onOrdersDataCallBack(JSONArray orders);
    }

    private MyAccountFragment mAccountFragment;

    public OrderAsyncTask(MyAccountFragment accountFragment) {
        mAccountFragment = accountFragment;
    }

    @Override
    protected JSONArray doInBackground(Void... voids) {
        GetOrdersAPI ordersAPI = new GetOrdersAPI();
        JSONArray ordersData = ordersAPI.getOrdersData();
        return ordersData;
    }

    @Override
    protected void onPostExecute(JSONArray orders) {
        super.onPostExecute(orders);
        mAccountFragment.onOrdersDataCallBack(orders);
    }

}
