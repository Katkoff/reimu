package reimu.edge.object.reimu.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import reimu.edge.object.reimu.R;


/**
 * Created by michaelkatkov on 4/5/18.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder> {

    private int[] imageIds;
    private View.OnClickListener onClickListener;

    public VideoAdapter(int[] imageIds, View.OnClickListener onClickListener) {
        this.imageIds = imageIds;
        this.onClickListener = onClickListener;
    }

    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_adapter, parent, false);
        return new VideoViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(VideoViewHolder holder, int position) {
        holder.imageView.setImageResource(imageIds[position]);
        holder.imageView.setOnClickListener(onClickListener);
    }


    @Override
    public int getItemCount() {
        return imageIds.length;
    }


    public class VideoViewHolder extends RecyclerView.ViewHolder
    {
        ImageView imageView;

        public VideoViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image_video_adapter);
        }
    }
}
