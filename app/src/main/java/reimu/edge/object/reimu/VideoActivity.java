package reimu.edge.object.reimu;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import reimu.edge.object.reimu.fragments.ReimuFragment;


/**
 * Created by michaelkatkov on 4/5/18.
 */

public class VideoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video);
        showReimuYoutubeFragment();
    }

    protected void showReimuYoutubeFragment() {

        ReimuFragment reimuFragment  = ReimuFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.video_content, reimuFragment)
                .commit();
    }
}
