package reimu.edge.object.reimu;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import reimu.edge.object.reimu.fragments.MyAccountFragment;


/**
 * Created by michaelkatkov on 4/5/18.
 */

public class AccountActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        showMyAccountFragment();
        findViewById(R.id.my_library).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    protected void showMyAccountFragment() {
        MyAccountFragment myAccountFragment  = MyAccountFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.account_content, myAccountFragment)
                .commit();
    }

}
