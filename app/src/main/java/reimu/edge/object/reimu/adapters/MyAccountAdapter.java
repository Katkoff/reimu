package reimu.edge.object.reimu.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import reimu.edge.object.reimu.R;
import reimu.edge.object.reimu.model.CommerceItems;
import reimu.edge.object.reimu.model.OrderDetailsModel;

/**
 * Created by owner on 3/13/18.
 */

public class MyAccountAdapter extends BaseAdapter{

    private LayoutInflater mLayoutInflater;
    private ArrayList<CommerceItems> mOrderDetailsModel;

    public MyAccountAdapter(LayoutInflater layoutInflater, ArrayList<CommerceItems> orderDetailsModels){
        mLayoutInflater = layoutInflater;
        mOrderDetailsModel = orderDetailsModels;
    }

    @Override
    public int getCount() {
       return mOrderDetailsModel.size();
    }

    @Override
    public Object getItem(int i) {
        return mOrderDetailsModel.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup convertView) {
        ViewHolder holder;

        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.fragment_my_account_page_row, null);
            holder = new ViewHolder();
            holder.subscriptionName = (TextView) view.findViewById(R.id.subsciptionName);
            holder.amount = (TextView) view.findViewById(R.id.amount);
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }
        holder.subscriptionName.setText(mOrderDetailsModel.get(position).getProductDisplayName());
        holder.amount.setText("$" +mOrderDetailsModel.get(position).getPriceInfo().getAmount());
        return view;
    }



    private static class ViewHolder  {
        private TextView subscriptionName;
        private TextView amount;

    }
}
