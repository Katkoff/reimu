package reimu.edge.object.reimu.seekbar;


/**
 * Created by michaelkatkov on 4/13/18.
 */

public interface TerminalChangedListener
{
    /**
     * @param terminal terminal that just got triggered on the seek bar
     */
    void onTerminalChanged(final Terminal terminal);
}
