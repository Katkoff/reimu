package reimu.edge.object.reimu.seekbar;


/**
 * Created by michaelkatkov on 4/13/18.
 */

public enum TerminalAnimationType
{
    BLINK_STILL,
    BLINK_ACTIVE
}
